/*
 * Cyclone V - FPGA Development Board
 * Terasic DE0-CV
 * データシート: https://www.marutsu.co.jp/contents/shop/marutsu/datasheet/P0192.pdf
 */
module CPU_DE0_CV_TOP (
    input  CLK,
    input  RST,
    input  [1:0] KEY,
    input  [4:0] SW,
    output [3:0] LEDR
);

/*
 * クロック分周モジュール 1Hz
 */
reg [25:0] cnt;
wire clk1Hz = (cnt == (26'd49_999_999));
always @(posedge CLK) begin
    if (RST) begin
        cnt <= 26'b0;
    end else if (clk1Hz) begin
        cnt <= 26'b0;
    end else begin
        cnt <= cnt + 26'b1;
    end
end

/*
 * 1Hzまたはマニュアルのクロックに切り替える
 */

/*
 * この回路はリセット信号を生成する
 */
wire reset_N;
assign reset_N = KEY[0];
//reset_switch reset0 (
////  .clk(CLK),
//    .in(KEY),
//    .out(reset_N)
//);

/*
 * ROM(Road Only Memory) 16byte.
 * 読み込み専用メモリ
 */
wire [7:0] inst;
wire [3:0] pc;
rom rom128 (
//  .clk(CLK),
    .addr(pc),
    .inst(inst)
);

/*
 * 4-bit CPU
 */
cpu cpu4 (
    .clk(CLK), // KEY[1], CLK
    .reset_N(reset_N),
    .inst(inst),
    .io_in(SW[3:0]),
    .io_out(LEDR),
    .pc(pc)
);

endmodule
