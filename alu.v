module alu (
    input  [3:0] in_a,
    input  [3:0] in_b,
    output [3:0] out,
    output cy
);

assign {cy, out} = in_a + in_b;

endmodule
