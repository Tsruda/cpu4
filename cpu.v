/*
 * 4-bit CPU
 */
module cpu (
    input  clk,
    input  reset_N,
    input  [7:0] inst,
    input  [3:0] io_in,
    output [3:0] io_out,
    output [3:0] pc
);

/*
 * ALU
 */
wire [3:0] alu_in_a, alu_in_b;
wire [3:0] alu_out;
wire alu_carry;
alu alu0 (
    .in_a(alu_in_a),
    .in_b(alu_in_b),
    .out(alu_out),
    .cy(alu_carry)
);

/*
 * Register A
 */
wire [3:0] reg_a_out;
wire load_a_N;
register reg_a (
    .clk(clk),
    .reset_N(reset_N),
    .load_N(load_a_N),
    .in(alu_out),
    .out(reg_a_out)
);

/*
 * Register B
 */
wire load_b_N;
wire [3:0] reg_b_out;
register reg_b (
    .clk(clk),
    .reset_N(reset_N),
    .load_N(load_b_N),
    .in(alu_out),
    .out(reg_b_out)
);

/*
 * Register I/O
 */
wire load_io_N;
register reg_io (
    .clk(clk),
    .reset_N(reset_N),
    .load_N(load_io_N),
    .in(alu_out),
    .out(io_out)
);

/*
 * Register PC
 */
wire load_pc_N;
reg  [3:0] fetch;
register reg_pc (
    .clk(clk),
    .reset_N(reset_N),
    .load_N(!clk),
    .in(fetch),
    .out(pc)
);

always @(*) begin
    if (!load_pc_N) begin
        fetch <= alu_out;
    end else begin
        fetch <= pc + 4'h1;
    end
end

// Carry Flag
reg carry_reg;
always @(posedge clk) begin
    if (alu_carry) begin
        carry_reg <= 1'b1;
    end  else begin
        carry_reg <= 1'b0;
    end
end

/*
 * Instruction Decoder
 */
wire [1:0] alu_sel;
decoder decode(
    .opcode(inst[7:4]),
    //.carry(alu_carry),
    .carry(carry_reg),
    .sel(alu_sel),
    .load0_N(load_a_N),
    .load1_N(load_b_N),
    .load2_N(load_io_N),
    .load3_N(load_pc_N)
);

/*
 * Selector
 */
wire [3:0] selected;
selector alu_select (
    .sel(alu_sel),
    .a(reg_a_out),
    .b(reg_b_out),
    .i(io_in),
    .y(selected)
);

assign alu_in_a = selected;
//assign alu_in_a = select(alu_sel);

assign alu_in_b = inst[3:0];

/*
function [3:0] select(
    input [3:0] sel
);
    begin
        case (sel)
            `SELECT_C0: select = reg_a_out;
            `SELECT_C1: select = reg_b_out;
            `SELECT_C2: select = io_in;
            `SELECT_C3: select = 4'h0;
            default:    select = 4'h0;
        endcase
    end
endfunction
*/

endmodule
