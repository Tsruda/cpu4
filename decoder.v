
`include "opcodes.vh"

module decoder (
    input [3:0] opcode,
    input carry,
    output [1:0] sel,
    output load0_N,
    output load1_N,
    output load2_N,
    output load3_N
);

wire [5:0] decoded;
assign decoded = decode(opcode, carry);

assign sel     = decoded[5:4];
assign load0_N = decoded[0];
assign load1_N = decoded[1];
assign load2_N = decoded[2];
assign load3_N = decoded[3];

function [5:0] decode (
    input [3:0] op,
    input carry_in
);
    begin
        case (op)
            // OPCODE    :            SELECT BA,  LD3,  LD2,  LD1,  LD0
            `OP_MOV_A_IM : decode = {`SELECT_C3, 1'b1, 1'b1, 1'b1, 1'b0};
            `OP_MOV_B_IM : decode = {`SELECT_C3, 1'b1, 1'b1, 1'b0, 1'b1};
            `OP_MOV_A_B :  decode = {`SELECT_C1, 1'b1, 1'b1, 1'b1, 1'b0};
            `OP_MOV_B_A :  decode = {`SELECT_C0, 1'b1, 1'b1, 1'b0, 1'b1};
            `OP_ADD_A_IM : decode = {`SELECT_C0, 1'b1, 1'b1, 1'b1, 1'b0};
            `OP_ADD_B_IM : decode = {`SELECT_C1, 1'b1, 1'b1, 1'b0, 1'b1};
            `OP_IN_A :     decode = {`SELECT_C2, 1'b1, 1'b1, 1'b1, 1'b0};
            `OP_IN_B :     decode = {`SELECT_C2, 1'b1, 1'b1, 1'b0, 1'b1};
            `OP_OUT_IM :   decode = {`SELECT_C3, 1'b1, 1'b0, 1'b1, 1'b1};
            `OP_OUT_B :    decode = {`SELECT_C1, 1'b1, 1'b0, 1'b1, 1'b1};
            `OP_JMP_IM :   decode = {`SELECT_C3, 1'b0, 1'b1, 1'b1, 1'b1};
            `OP_JNC_IM :
                if (carry_in) begin
                           decode = {`SELECT_C3, 1'b1, 1'b1, 1'b1, 1'b1};
                end else begin
                           decode = {`SELECT_C3, 1'b0, 1'b1, 1'b1, 1'b1};
                end
            default :      decode = {`SELECT_C3, 1'b1, 1'b1, 1'b1, 1'b1};
        endcase
    end
endfunction

endmodule
