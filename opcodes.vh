
`define SELECT_C0 2'b00 // Aレジスタ
`define SELECT_C1 2'b01 // Bレジスタ
`define SELECT_C2 2'b10 // IO
`define SELECT_C3 2'b11 // ZERO

/*
 * アセンブラ命令
 */

// 転送命令
`define OP_MOV_A_IM 4'b0011 //  Im  AレジスタにImを転送
`define OP_MOV_B_IM 4'b0111 //  Im  BレジスタにImを転送
`define OP_MOV_A_B  4'b0001 // 0000 AレジスタにBレジスタを転送
`define OP_MOV_B_A  4'b0100 // 0000 BレジスタにAレジスタを転送
// 演算命令
`define OP_ADD_A_IM 4'b0000 //  Im  AレジスタにImを加算
`define OP_ADD_B_IM 4'b0101 //  Im  BレジスタにImを加算
// I/O命令
`define OP_IN_A     4'b0010 // 0000 入力ポートからAレジスタへ転送
`define OP_IN_B     4'b0110 // 0000 入力ポートからBレジスタへ転送
`define OP_OUT_IM   4'b1011 //  Im  出力ポートへImを転送
`define OP_OUT_B    4'b1001 // 0000 出力ポートへBレジスタを転送
// ジャンプ命令
`define OP_JMP_IM   4'b1111 //  Im  Im番地へジャンプ
`define OP_JNC_IM   4'b1110 //  Im  Cフラグが1ではないときにジャンプ
// 擬似命令
`define OP_NOP {`OP_ADD_A_IM, 4'b0000}
