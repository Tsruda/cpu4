module register(
    input clk,
    input reset_N,
    input load_N,
    input  [3:0] in,
    output [3:0] out
);

reg [3:0] regs;

assign out = regs;

always @(posedge clk or negedge reset_N) begin
    if (!reset_N) begin
        regs <= 4'h0;
    end else if (!load_N) begin
        regs <= in;
    end /* else begin
        regs <= regs;
    end */
end

endmodule
