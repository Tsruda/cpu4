
module reset_switch(
    //input clk,
    input in,
    output out
);

assign out = ~in;

/*
 * MEMO: 順序回路の場合はリセットに2クロック使うため組み合わせ回路を使用
 */
//reg [3:0] buffer = 4'hf;
//always @(posedge clk) begin
//    buffer <= {buffer[3:1], in};
//end

//assign out = ~buffer;

endmodule
