`include "opcodes.vh"

module rom (
    input  [3:0] addr,
    output [7:0] inst
);

    assign inst = rom_data(addr);

    function [7:0] rom_data (
        input [3:0] rom_addr
    );
        begin
            case (rom_addr)
                4'b0000: rom_data = {`OP_OUT_IM, 4'b1111};
                4'b0001: rom_data = {`OP_OUT_IM, 4'b0101};
                4'b0010: rom_data = {`OP_OUT_IM, 4'b1010};
                4'b0011: rom_data = {`OP_OUT_IM, 4'b1001};
                4'b0100: rom_data = {`OP_OUT_IM, 4'b0110};
                4'b0101: rom_data = {`OP_OUT_IM, 4'b1001};
                4'b0110: rom_data = {`OP_OUT_IM, 4'b0110};
                4'b0111: rom_data = {`OP_OUT_IM, 4'b0001};
                4'b1000: rom_data = {`OP_OUT_IM, 4'b0010};
                4'b1001: rom_data = {`OP_OUT_IM, 4'b0100};
                4'b1010: rom_data = {`OP_OUT_IM, 4'b1000};
                4'b1011: rom_data = {`OP_OUT_IM, 4'b1100};
                4'b1100: rom_data = {`OP_OUT_IM, 4'b1110};
                4'b1101: rom_data = {`OP_OUT_IM, 4'b1111};
                4'b1110: rom_data = {`OP_OUT_IM, 4'b0000};
                4'b1111: rom_data = {`OP_JMP_IM, 4'b0000};
                default: rom_data = {`OP_NOP};
            endcase
        end
    endfunction

endmodule
