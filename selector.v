`include "opcodes.vh"

module selector (
    input  [1:0] sel,
    input  [3:0] a,
    input  [3:0] b,
    input  [3:0] i,
    output [3:0] y
);

assign y = select(sel, a, b, i);

function [3:0] select;
    input [1:0] sel;
    input [3:0] a;
    input [3:0] b;
    input [3:0] i;
    begin
        case (sel)
            `SELECT_C0: select = a;
            `SELECT_C1: select = b;
            `SELECT_C2: select = i;
            `SELECT_C3: select = 4'h0;
            default: select = 4'h0;
        endcase
    end
endfunction

endmodule
