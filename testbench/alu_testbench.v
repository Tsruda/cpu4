/*
 * ALU TestBench
 *
 * 真理値表
 * +-------------------+-------------------+-----------+
 * |       Input       |      Output       |           |
 * +---------+---------+-------------------+           |
 * |   In1   |   In2   |  Carry  |   Out   |           |
 * +---------+---------+---------+---------+-----------+
 * | 4'b0000 | 4'b0000 | 1'b0    | 4'b0000 | MIN + MIN |
 * | 4'b0000 | 4'b1111 | 1'b0    | 4'b1111 | MIN + MAX |
 * | 4'b1111 | 4'b0000 | 1'b0    | 4'b1111 | MIN + MAX |
 * | 4'b1111 | 4'b1111 | 1'b1    | 4'b1110 | MIN + MAX |
 * | 4'bxxxx | 4'bxxxx | ----    | ------- |           |
 * | 4'b0000 | 4'bxxxx | ----    | ------- |           |
 * | 4'bxxxx | 4'b0000 | ----    | ------- |           |
 * | 4'b0000 | 4'bzzzz | ----    | ------- |           |
 * | 4'bzzzz | 4'b0000 | ----    | ------- |           |
 * +---------+---------+---------+---------+-----------+
 */

`timescale 1ps/1ps

module alu_testbench;

reg [3:0] input_1, input_2;
wire [3:0] result;
wire carry;

alu aluTest (
    .in_a(input_1),
    .in_b(input_2),
    .out(result),
    .cy(carry)
);

parameter CYCLE      = 100;
parameter HALF_CYCLE = (CYCLE / 2);

reg clk;
always begin
    clk = 1; #(HALF_CYCLE);
    clk = 0; #(HALF_CYCLE);
end

/*
 */
initial begin
    input_1 <= 4'h0;
    input_2 <= 4'h0;

#CYCLE
    input_1 <= 4'h1; input_2 <= 4'h1;

#CYCLE
    input_1 <= 4'h0;

#CYCLE
    input_2 <= 4'h0;

#CYCLE
    input_1 <= 4'hf;  input_2 <= 4'hf;

#CYCLE
    input_2 <= 4'h0;

#CYCLE
    input_2 <= 4'h1;

repeat(CYCLE * 4)
    $finish;
end

initial begin
    $monitor($stime, " in0:%h, in1:%h, carry:%b, out:%b",
        input_1, input_2, carry, result);
end

//initial begin
//    $dumpfile("alu.vcd");
//    $dumpvars(0, aluTest);
//end

endmodule
