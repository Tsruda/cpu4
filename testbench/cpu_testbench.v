module cpu_testbench;

reg clk;
reg push_sw;
reg [3:0] in;
wire [3:0] out;

wire reset_N;
reset_switch reset (
    .in(push_sw),
    .out(reset_N)
);

wire [3:0] pc;
wire [7:0] inst;
rom rom128 (
    .addr(pc),
    .inst(inst)
);

cpu cpu4 (
    .clk(clk),
    .reset_N(reset_N),
    .inst(inst),
    .io_in(in),
    .io_out(out),
    .pc(pc)
);

initial begin
    clk = 0;
    forever #1 clk = !clk;
end

initial begin
    push_sw = 1'b0;
    in = 1'b1;
    @(posedge clk)
    reset_button();
    repeat(17) @(posedge clk);
    $finish;
end

task reset_button;
    begin
        push_sw = 1'b1; // active-high
        repeat(1) @(posedge clk);
        push_sw = 1'b0;
    end
endtask

initial begin
    $monitor("clk:%b, reset_N:%b, inst:%b, in:%h, out:%h, pc:%h",
        clk, reset_N, inst, in, out, pc);
end

initial begin
    $dumpfile("cpu_test.vcd");
    $dumpvars(0, cpu4);
end

endmodule
