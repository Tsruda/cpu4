
`include "opcodes.vh"

`timescale 1ps/1ps

module de0_cv_top_testbench;

reg clk;
reg rst; // = 1'b0;
reg [1:0] push_sw;
reg [4:0] dip_sw; //= 4'b0;
wire [3:0] led;

parameter CYCLE     = 100;
parameter HALFCYCLE = 50;
parameter DELAY     = 10;

CPU_DE0_CV_TOP cpu4bit (
    .CLK(clk),
    .RST(rst),
    .KEY(push_sw),
    .SW(dip_sw),
    .LEDR(led)
);

//initial begin
//    clk = 1'b0;
//    forever #CYCLE clk = !clk;
//end

always begin
    clk = 1; #HALFCYCLE;
    clk = 0; #HALFCYCLE;
end

/*
 * MEMO: 2019/02/25 遅延テスト
 * 期待動作: クロック立ち上がり時にフェッチする
 * 結果動作: リセット回路信号の値によって切り替わる(組み合わせ回路のため)
 * 修正対応: 現組み合わせ回路(デコーダ・ROM・ALUセレクタ)をクロックの立ち上がりに同期
 */

initial begin
#DELAY
#(CYCLE)     // @(posedge clk);
    reset_button();
#(CYCLE * 4) // repeat(4) @(posedge clk);
    reset_button();
#(CYCLE * 8) // repeat(8) @(posedge clk);
    reset_button();
    $finish();
end

/*
 * MEMO: 2019/02/25
 * repeat(1)ではリセット回路のアクティブ状態を波形による確認ができない
 */
task reset_button;
    begin
        push_sw = 1'b0; // reset on
        repeat(2) @(posedge clk);
        push_sw = 1'b1; // reset off
    end
endtask

initial begin
    $monitor($stime, " clk:%d, reset:%b, led:%b, pc:%b, inst:%b", clk, push_sw, led, cpu4bit.pc, cpu4bit.inst);
end

/*
 * 2019/02/25 波形による確認
 */
initial begin
    $dumpfile("result.vcd");
    $dumpvars(0, cpu4bit);
end

endmodule
