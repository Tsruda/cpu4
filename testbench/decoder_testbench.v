module decoder_testbench;

reg [3:0] op;
reg c;
wire [1:0] select;
wire load_a, load_b, load_i, load_z;

decoder decodeTest(
    .opcode(op),
    .carry(c),
    .sel(select),
    .load0_N(load_a),
    .load1_N(load_b),
    .load2_N(load_i),
    .load3_N(load_z)
);

reg clk;
initial begin
    clk = 1'b0;
    forever #1 clk = !clk;
end

initial begin
    c  <= 1'b0;
    @(posedge clk)
    op <= 4'b0011; // MOV A, Im
    @(posedge clk)
    op <= 4'b0111; // MOV B, Im
    @(posedge clk)
    op <= 4'b0001; // MOV A, B
    @(posedge clk)
    op <= 4'b0100; // MOV B, A
    @(posedge clk)
    op <= 4'b0000; // ADD A, Im
    @(posedge clk)
    op <= 4'b0101; // ADD B, Im
    @(posedge clk)
    op <= 4'b0010; // IN A
    @(posedge clk)
    op <= 4'b0110; // IN B
    @(posedge clk)
    op <= 4'b1011; // OUT Im
    @(posedge clk)
    op <= 4'b1001; // OUT B
    @(posedge clk)
    op <= 4'b1111; // JMP Im
    @(posedge clk)
    op <= 4'b1110; // JNC Im
    @(posedge clk)
    c  <= 1'b1;

    $finish;
end

initial begin
    $monitor("[%t] op:%b, sel:%b, abcd:%b %b %b %b, c:%b",
              $time, op, select, load_a, load_b, load_i, load_z, c);
end

//initial begin
//    $dumpfile("decoder_testbench.vcd");
//    $dumpvars(0, decodeTest);
//end

endmodule
