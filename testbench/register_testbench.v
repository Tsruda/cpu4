module register_testbench;

reg clk, reset_N, load_N;
reg [3:0] indata;
wire [3:0] outdata;

register regTest(.clk(clk)
            , .reset_N(reset_N)
            , .load_N(load_N)
            , .in(indata)
            , .out(outdata)
);

initial begin
    clk = 1'b0;
    forever #1 clk = !clk;
end

initial begin
    reset_N = 1'b1;

    @(posedge clk)
    reset_button();

    @(posedge clk)
    $display("入力信号:Enable, 読み込み信号:Disable");
    indata = 4'h1;

    @(posedge clk)
    $display("入力信号:Enable, 読み込み信号:Enable");
    load_enable();

    @(posedge clk)
    $display("入力信号:Change");
    indata = 4'h2;

    @(posedge clk)
    $display("入力信号:Enable, 読み込み信号 Disable");
    load_disable();
    $display("入力信号:Change");
    indata = 4'h1;

    @(posedge clk)
    reset_button();

    $finish;
end

task reset_button;
    begin
        $display("リセット信号:Enable");
        reset_N = 1'b0; // reset on active-low
        repeat(1) @(posedge clk);
        reset_N = 1'b1; // reset off
    end
endtask

task load_enable;
    begin
        load_N = 1'b0; // active-low
    end
endtask

task load_disable;
    begin
        load_N = 1'b1; // active-low
    end
endtask

initial begin
    $monitor("[%t] clk:%b, reset_N:%b, load_N:%b, in:%h, out:%h, reg:%h",
              $time, clk, reset_N, load_N, indata, outdata, regTest.regs);
end

/*
initial begin
    $dumpfile("register_testbench.vcd");
    $dumpvars(0, regTest);
end
*/

endmodule
