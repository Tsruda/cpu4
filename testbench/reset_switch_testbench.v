

module reset_switch_testbench;

reg clk;
reg in;
wire out;

reset_switch reset (
    .in(in),
    .out(out)
);

initial begin
    clk = 1'b0;
    forever #1 clk = !clk;
end

initial begin
    in = 1'b0;
    repeat(5) @(posedge clk);
    reset_button();
    repeat(5) @(posedge clk);
    $finish();
end

task reset_button;
    begin
        in = 1'b1;
        repeat(15) @(posedge clk);
        in = 1'b0;
    end
endtask

initial begin
    $monitor("clk:%b, in:%b, out:%b", clk, in, out);
end

endmodule
