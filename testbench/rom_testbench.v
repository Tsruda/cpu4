module rom_testbench;

reg [3:0] addr;
wire [7:0] inst;

rom rom128 (
    .addr(addr),
    .inst(inst)
);

reg clk;
initial begin
    clk = 1'b0;
    forever #1 clk = !clk;
end

initial begin
    addr = 4'b0000;
    repeat(16) begin
        @(posedge clk)
        addr = addr + 1'b1;
    end
    $finish;
end

initial begin
    $monitor("[%t] clk:%b, addr:%b, inst:%b",
              $time, clk, addr, inst);
end

initial begin
    $dumpfile("rom.vcd");
    $dumpvars(0, rom128);
end

endmodule
